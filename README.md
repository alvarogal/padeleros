# Padeleros

Trabajo de Fin de Grado para UNIR

**Descripción**
<br>
Drupal 7 con Commerce y los módulos necesarios para convertir el CMS en una página de comercio electrónico especializada en materiales deportivos del mundo del pádel.

**Autoría**

Alumno: Álvaro Galán Galindo
<br>
Módulo: Trabajo de Fin de Grado
<br>
Curso lectivo: 2018/2019
